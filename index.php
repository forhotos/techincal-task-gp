<!DOCTYPE html>
<html>
    <head>
        <title>Тестовое задание Генератор продаж</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="styles.css" />
        <style>
            @import url('https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300..800;1,300..800&family=Varela&display=swap');
        </style>     
        <script defer src="popup-script.js"></script>        
        <script defer src="phone-mask.js"></script>     
    </head>
    <body>
        <div class="main">
            <button class="main-button" id="popupOpen" onclick="openPopup()">
                <div class="main-button-label">Кликни</div>
            </button>
        </div>
        <div id="popup" class="modal">
            <div class="modal-content">
              <div class="modal-content-header">
                <button id="popupClose" onclick="closePopup()">
                    <img src="assets/close.png" alt="close">
                </button>
              </div>
              <div class="modal-content-body">
                <div class="modal-content-body-half">
                    <div class="form-title">
                        Получите набор файлов для руководителя:
                    </div>
                    <div class="form-images">
                        <div class="form-images-item">
                            <img id="icom" src="assets/icom.png" alt="icom" width="360">
                        </div>
                        <img src="assets/file.png" alt="file" id="file">
                    </div>
                    
                </div>
                <div class="modal-content-body-half">
                    <div class="form-wrapper">
                        <form class="form" id="form" action="submitData.php" method="post">
                            <div class="form-item">
                                <label>Введите Email для получения файлов:</label>
                                <input 
                                    type="email" 
                                    placeholder="E-mail" 
                                    class="form-input" 
                                    id="email"
                                    name="email"
                                >
                            </div>
                            <div class="form-item">
                                <label>Введите телефон для подтверждения доступа:</label>
                                <input 
                                    type="text" 
                                    placeholder="+7 (000) 000-00-00" 
                                    class="form-input" 
                                    id="phone"
                                    name="phone"
                                    required
                                    data-js="input"
                                >
                            </div>
                            <div class="form-item">
                                <button id="submit" type="submit">
                                    <div id="submitLabel" class="">Скачать файлы</div>
                                    <img src="assets/submit.png" alt="submit" id="submitIcon">
                                </button>
                            </div>
                            <div id="annotation">
                                <div>
                                    PDF 4,7 MB
                                </div>
                                <div>
                                    DOC 0,8 MB
                                </div>
                                <div>
                                    XLS 1,2 MB
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
              </div>
            </div>
          </div>
    </body>
</html>