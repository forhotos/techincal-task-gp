const popup = document.getElementById("popup");
window.onclick = function(event) {
  if (event.target == popup) {
    closePopup();
  }
}

function openPopup() {
    popup.style.display = "flex";
}

function closePopup() {
    popup.style.display = "none";
}

function formSubmit(e){
    e.preventDefault();

    alert("Ваша заявка принята!");
}