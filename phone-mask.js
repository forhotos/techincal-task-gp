const phoneInput = document.getElementById('phone');
phoneInput.addEventListener("input", (e) => {
  const value = phoneInput.value.replace(/\D+/g, "");
  const numberLength = 11;

  let result;
  result = "+";

  //
  for (let i = 0; i < value.length && i < numberLength; i++) {
    switch (i) {
      case 0:
        result += prefixNumber(value[i]);
        continue;
      case 4:
        result += ") ";
        break;
      case 7:
        result += "-";
        break;
      case 9:
        result += "-";
        break;
      default:
        break;
    }
    result += value[i];
  }
  phoneInput.value = result;
});
const prefixNumber = (str) => {
  if (str === "9") {
    return "7 (9";
  }
  return "7 (";
};